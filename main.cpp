#include <iostream>
#include <string>

int main(int argc, char const *argv[])
{
    std::string name;
    int number1, number2;
    std::cout << "Please input your name: ";
    std::cin >> name;
    std::cout << "Hi " << name << ", hope you are doing well.\nNice to see you using this very simple C++ program.\nThis program is not the main point here. Main point is to demonstrate usage of Docker. Have a nice rest of the day, " << name << "!\n\n\n\n\n" << std::endl;
    std::cout << "Psst. I just learned to multiply... Want to see ?\nPlease enter two numbers: ";
    std::cin >> number1;
    std::cin >> number2;
    int result = number1 * number2;
    std::cout << number1 << " times " << number2 << " = " << result << std::endl;
    return 0;
}