FROM gcc:11

RUN apt-get update --yes
# uncomment line below if you're using macOS
# RUN apt-get install --yes clang 

COPY . /usr/src/part_two_demo
WORKDIR /usr/src/part_two_demo

#comment the line you are not using. 
RUN g++ main.cpp -o main
#RUN clang++ main.cpp -o main

CMD ["./main"]
