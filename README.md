## NAME
Docker demo

## DESCRIPTION
This program is part of Lesson Task:2.2

## PART 3 of the task
TLDR;
Air quality sensor gathering data on a roof of tall building down town area of a bigger city. It's data gathering script is still accessible from comfort of eg. office.

One unique use of Docker. With Docker you can access and run programs on embedded devices that are located in 'hard-to-reach' locations. As example weather monitors, like air quality sensor. Air quality sensor can be located anywhere and when it's application is running on Docker it is still accessible from pretty much anywhere you can imagine. 

## AUTHOR
Tino Nummela